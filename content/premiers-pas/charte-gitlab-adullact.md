---
title: "Charte d'utilisation"
date: 2018-07-05T18:15:26+02:00
weight: 10
---

Charte d'utilisation du Gitlab Adullact

## Conditions d'utilisation

En utilisant Gitlab.adullact.net, vous reconnaissez être lié par les termes des présentes conditions d'utilisation de la plate-forme.

## Description du service

Gitlab.adullact.net est un service d'hébergement et de travail collaboratif consacré au développement de logiciels libres métiers. Les services offerts en ligne vont de l'hébergement versionné du code source en passant par des outils de communication tel suivi de tickets, wiki, intégration continue.

## Ouverture d'un projet

Toute personne peut ouvrir un compte, et proposer un projet. L'Adullact se réserve le droit de refuser l'hébergement d'un projet qui ne présenterait pas d'intérêt pour la sphère de l'argent public au sens large, ou/et qui ne serait pas sous une licence GPL-compatible ou reconnue par l'OSI. Chaque projet est sous la responsabilité de l'administrateur à l'initiative du dépôt sur Gitlab.adullact.net. Il est libre d'allouer et organiser les moyens et les ressources utiles au développement et au support du projet.

## Accès à la plate-forme

L'accès à Gitlab.adullact.net est public. Toute personne peut visualiser, télécharger, réutiliser, les éléments en ligne, dans le respect des licences associées à ces éléments.

## Responsabilité relative au contenu

Tout contenu, c'est-à-dire information, donnée, texte, logiciel, musique, son, photographie, vidéo, message ou matériau de quelque type que ce soit, transmis et conservé sur Gitlab.adullact.net, est sous la seule responsabilité de la personne à l'origine du dépôt. L'Adullact ne saurait être responsables des contenus déposés sur Gitlab.adullact.net. L'Adullact s'engage à retirer tout contenu illicite conformément aux dispositions de la loi n°2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique. Les ressources collaboratives ne peuvent être utilisées comme supports de publicité ou de prospection commerciale.

## Retrait d'un projet

Le retrait d'un projet est admis, sur demande unanime de ses administrateurs. L'ensemble des traces, données, documents, relatifs au projet sont alors remis aux administrateurs. Il pourra être mis fin au compte Gitlab.adullact.net d'une personne pour les raisons suivantes, sans que cette liste soit limitative :

* En cas d'agissement détournant l'usage ou mettant en péril la plate-forme
* En cas de violation de lois en vigueur et à la demande de la Justice

## Limitation de garantie

L'Adullact ne donne aucune garantie de service en termes de disponibilité du service et de préservations des données. L'Adullact s'engage engage à faire opérer le service de la forge Gitlab.adullact.net en accord avec l'état de l'art, particulièrement en terme de conservation de l'intégrité des données stockées.
