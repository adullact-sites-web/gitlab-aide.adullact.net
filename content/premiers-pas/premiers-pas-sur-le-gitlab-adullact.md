---
title: "Premiers pas sur le Gitlab Adullact"
date: 2018-07-05T17:25:03+02:00
weight: 10
---

##  GitLab

GitLab est une forge logicielle, sous licence Libre. Il s'agit d'un gestionnaire de dépôts de code source sous Git, et offre entre autres fonctionnalités : le suivi d'évolutions (*issues*), les wikis, les demandes de fusion (*merge request*).

## Se créer un compte sur le GitLab Adullact

Pour se connecter au [GitLab Adullact](https://gitlab.adullact.net/), il convient d'utiliser les mêmes identifiants que pour le [FusionForge (adullact.net)](https://adullact.net/).

Note : pour avoir son compte FusionForge activé sur le Gitlab, il faut s'être connecté sur le [FusionForge](https://adullact.net/) au moins une fois depuis septembre 2016.

## Passer l'interface GitLab en français

Note janvier 2018 : la [traduction de GitLab](https://docs.gitlab.com/ee/development/i18n/) est en cours, plusieurs chaînes de caractères peuvent être encore en anglais.

Pour changer la langue de l'interface, il convient de :

1. Se connecter au [GitLab Adullact](https://gitlab.adullact.net/) ;
2. Se rendre sur son profil (icône en haut à droite) puis cliquer sur *Settings* ;
3. Sur la page affichée, dans la rubrique *Main settings*, choisir Français pour *Preferred Language*.

## Comment créer un projet privé sur le GitLab Adullact

Des projets privés peuvent être ouverts sur demande. Pour cela, il suffit d'adresser sa demande à [support AROBASE adullact.org](mailto:support AROBASE adullact.org).

## FusionForge adullact.net et éventuelle migration

Le site [adullact.net](https://adullact.net/) (motorisé par le [logiciel libre FusionForge](http://fusionforge.org/)) reste en place ; le GitLab arrivant en ajout de FusionForge.

* Il n'est pas prévu de migration automatique depuis le FusionForge vers la GitLab.
* Une migration manuelle est laissée à la discrétion de chacun des responsables de projets.
* Les nouveaux projets Git sont invités à se placer sur le GitLab.
* Nous proposons d'accompagner nos adhérents sur Git / GitLab / la création de communautés autour de leurs logiciels libres.

